
provider "aws" {
  region = "eu-west-3"
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  name = "vpc-RN"
  cidr = "10.0.0.0/16"
  azs             = ["eu-west-3a", "eu-west-3b"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24"]

  enable_nat_gateway = true
  enable_dns_support   = true
  enable_dns_hostnames = true
  single_nat_gateway   = false
  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}


module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "~> 19.20.0"

  cluster_name    = "Cluster-ProjetFRG"
  cluster_version = "1.27"

  cluster_endpoint_public_access  = true
  cluster_endpoint_private_access  = true
  vpc_id = module.vpc.vpc_id 
  subnet_ids = module.vpc.private_subnets   
  cluster_security_group_id = aws_security_group.eks_cluster_sg.id
  enable_irsa = true
  eks_managed_node_group_defaults = { 
    disk_size = 40 
    }

 
  eks_managed_node_groups = {
    Node-ProjetFRG = {
      min_size     = 1
      max_size     = 2
      desired_size = 1

      instance_types = ["t2.medium"]
      capacity_type  = "ON_DEMAND"
    }
  }
}

resource "aws_security_group" "eks_cluster_sg" {
  name        = "projetFRG-eks-security-group"
  description = "Cluster communication with worker nodes"
  vpc_id      = module.vpc.vpc_id
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups = [module.eks.node_security_group_id] 
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    security_groups = [module.eks.node_security_group_id] 
  }

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}


resource "aws_db_subnet_group" "rds_subnet_group" {
  name       = "rds-subnet-group"
  subnet_ids = module.vpc.private_subnets
}

resource "aws_security_group" "rds_sg" {
  name        = "rds-sg"
  description = "Security group for RDS instance"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    security_groups = [
      module.eks.cluster_security_group_id,
      module.eks.node_security_group_id,
      aws_security_group.eks_cluster_sg.id,
      # Add more security groups if you have additional node groups
    ]
  }
}





resource "aws_db_instance" "rds_db" {
  identifier            = "database-projetfrg"
  allocated_storage     = 20
  storage_type          = "gp2"
  engine                = "postgres"
  instance_class        = "db.t3.micro"
  username              = "fastapi_traefik"
  password              = "admin123"
  publicly_accessible   = true
  vpc_security_group_ids = [aws_security_group.rds_sg.id]
  db_subnet_group_name  = aws_db_subnet_group.rds_subnet_group.name
  availability_zone     = "eu-west-3a"
  skip_final_snapshot   = true
}



resource "null_resource" "kubectl" {
       depends_on = [module.eks]
       provisioner "local-exec" {
          command = "aws eks --region eu-west-3 update-kubeconfig --name Cluster-ProjetFRG"
          }
}

provider "kubernetes" {
  config_path = "~/.kube/config"
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}



resource "helm_release" "traefik" {
  name       = "traefik"
  repository = "https://helm.traefik.io/traefik"
  chart      = "traefik"
  namespace  = "traefik"
  create_namespace = true

  values = [
    file("values.yaml")  # Reference to values.yaml in the root folder
  ]
  depends_on = [null_resource.kubectl]
}


# Helm Release for the entire chart
resource "helm_release" "entire_chart" {
  name       = "projetfrg" 
  chart      = "./charts-prod"
  namespace  = "dev"
  create_namespace = true
  values = [file("charts-prod/values.yaml")]
  set {
    name  = "rdsService.externalName"
    value = substr(aws_db_instance.rds_db.endpoint, 0, length(aws_db_instance.rds_db.endpoint) - 5)
  }
  depends_on = [ helm_release.traefik,
aws_db_instance.rds_db]
}

