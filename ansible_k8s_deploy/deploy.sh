#!/bin/bash

# Installer les dépendances nécessaires si elles ne sont pas déjà installées
ansible-galaxy install -r requirements.yml

# Exécuter le playbook Ansible
ansible-playbook -i hosts.ini deploy_k8s.yml
